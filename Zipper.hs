module Zipper where

	data Tree a = Node a (Tree a) (Tree a) | Empty deriving Show
	data Crumb a = LeftCrumb a (Tree a) | RightCrumb a (Tree a) deriving Show
	data LZipr a = LZipr ([a],[a]) deriving Show
	data TZipr a = TZipr (Tree a, [Crumb a]) deriving Show

	class LZipper z where
		getL :: z a -> [a]
		getR :: z a -> [a]
		moveLeft :: z a -> z a
		moveRight :: z a -> z a
		lmap :: (a -> a) -> z a -> z a
		rmap :: (a -> a) -> z a -> z a
		zmap :: (a -> a) -> z a -> z a

	instance LZipper LZipr where
		getL (LZipr (x,y)) = x
		getR (LZipr (x,y)) = y
		moveLeft (LZipr ((xs), (y:ys))) = LZipr ((y:xs), ys)
		moveRight (LZipr ((x:xs), (ys))) = LZipr ((xs), (x:ys))
		lmap f (LZipr (xs, ys)) = LZipr (map f xs, ys)
		rmap f (LZipr (xs, ys)) = LZipr (xs, map f ys)
		zmap f z = rmap f (lmap f z) 


	class BTZipper z where
		getLT :: z a -> Tree a
		getRT :: z a -> [Crumb a]
		moveLeftT :: z a -> z a
		moveRightT :: z a -> z a
		moveUp :: z a -> z a
		(-->) :: z a -> (z a -> z a) -> z a

	instance BTZipper TZipr where
		getLT (TZipr (x,y)) = x
		getRT (TZipr (x,y)) = y
		moveLeftT (TZipr (Node x l0 r0, bs)) = TZipr (l0, LeftCrumb x r0:bs)
		moveRightT (TZipr (Node x l0 r0, bs)) = TZipr (r0, RightCrumb x l0:bs)
		moveUp (TZipr (Node x1 l1 r1, LeftCrumb x0 r0:bs)) = TZipr ((Node x0 (Node x1 l1 r1) r0), bs)
		moveUp (TZipr (Node x1 l1 r1, RightCrumb x0 l0:bs)) = TZipr ((Node x0 l0 (Node x1 l1 r1)), bs)
		(-->) z f = f z
